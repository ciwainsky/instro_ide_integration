# InstRO IDE Integration README

InstRO - a framework for building tailored compiler instrumentation.
InstRO_IDE_Integration Repository includes projects for different IDE's and is intended to be integrated as a submodule into the InstRO base directory ./IDE

For Licence information refer to licence.md